package main

import (
	"net/http"
)

// Client - just a decorator interface
type Client interface {
	Do(*http.Request) (*http.Response, error)
}

// ClientFunc is a function type that implemets the Client interface
type ClientFunc func(*http.Request) (*http.Response, error)

//Do does the request
func (f ClientFunc) Do(r *http.Request) (*http.Response, error) {
	return f(r)
}

// Decorate decorates a Client c with all the given Decorators, in order.
func Decorate(c Client, ds ...Decorator) Client {
	decorated := c
	for _, decorate := range ds {
		decorated = decorate(decorated)
	}

	return decorated
}

// A Decorator wraps a Client with extra behaviour.
type Decorator func(Client) Client

// Authorization returns a Decorator that authorizes every Client request with the given token
func Authorization(token string) Decorator {
	return Header("Authorization", token)
}

// Header sdsd
func Header(name, value string) Decorator {
	return func(c Client) Client {
		return ClientFunc(func(r *http.Request) (*http.Response, error) {
			r.Header.Add(name, value)
			return c.Do(r)
		})
	}
}
