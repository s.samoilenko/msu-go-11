package main

// A Decorator wraps a Client with extra behaviour.
type Decorator func(Client) Client
