package main

import (
	"net/http"
)

// Client - just a decorator interface
type Client interface {
	Do(*http.Request) (*http.Response, error)
}
