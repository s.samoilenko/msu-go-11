package main

import (
	"net/http"
)

// ClientFunc is a function type that implemets the Client interface
type ClientFunc func(*http.Request) (*http.Response, error)

//Do does the request
func (f ClientFunc) Do(r *http.Request) (*http.Response, error) {
	return f(r)
}
