package commands

// CommandHandle a common handle
type CommandHandle interface {
	Handle(params []string) string
}
