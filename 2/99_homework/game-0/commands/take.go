package commands

import (
	"fmt"

	"mail.ru/msu-go-11/2/99_homework/game-0/model"
)

// Take a thing
type Take struct {
	InventoryList []*model.Inventory
	Player        *model.Player
}

// Handle move a player
func (t *Take) Handle(params []string) string {
	if t.Player.Backpack == nil {
		return "некуда класть"
	}
	var inventoryToWear *model.Inventory
	for _, tmp := range t.InventoryList {
		if params[0] == tmp.Title {
			inventoryToWear = tmp
			break
		}
	}

	if inventoryToWear == nil || t.Player.Location.HasInventory(inventoryToWear) == false {
		return "нет такого"
	}

	if inventoryToWear.CanTake() == false {
		return fmt.Sprintf("%s нельзя взять", params[0])
	}
	t.Player.Location.RemoveInventory(inventoryToWear)
	t.Player.Put(inventoryToWear)

	return fmt.Sprintf("предмет добавлен в инвентарь: %s", params[0])
}
