package commands

import (
	"fmt"

	"mail.ru/msu-go-11/2/99_homework/game-0/model"
)

// Apply put on a thing to a player
type Apply struct {
	InventoryList []*model.Inventory
	Player        *model.Player
}

// Handle move a player
func (a *Apply) Handle(params []string) string {
	var inventoryToApply *model.Inventory
	for _, tmp := range a.InventoryList {
		if params[0] == tmp.Title {
			inventoryToApply = tmp
			break
		}
	}

	if a.Player.HasInventory(inventoryToApply) == false {
		return fmt.Sprintf("нет предмета в инвентаре - %s", params[0])
	}

	fourniture, ok := a.Player.Location.GetFurnitureList()[params[1]]

	if !ok {
		return "не к чему применить"
	}

	if inventoryToApply == nil {
		return fmt.Sprintf("не такого инвентаря %s", params[0])
	}

	return fourniture.Apply(inventoryToApply)
}
