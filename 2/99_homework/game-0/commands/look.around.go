package commands

import (
	"mail.ru/msu-go-11/2/99_homework/game-0/model"
)

// LookAround a player
type LookAround struct {
	Player *model.Player
}

// Handle do it
func (l *LookAround) Handle(params []string) string {
	return l.Player.LookAround()
}
