package commands

import (
	"fmt"

	"mail.ru/msu-go-11/2/99_homework/game-0/model"
)

// PutOn put on a thing to a player
type PutOn struct {
	InventoryList []*model.Inventory
	Player        *model.Player
}

// Handle move a player
func (p *PutOn) Handle(params []string) string {
	if err := p.Player.PutOn(p.InventoryList, params); err != nil {
		return err.Error()
	}

	return fmt.Sprintf("вы одели: %s", params[0])
}
