package commands

import (
	"fmt"

	"mail.ru/msu-go-11/2/99_homework/game-0/model"
)

// Walk go to somewhere
type Walk struct {
	LocationList []model.LocationActioner
	Player       *model.Player
}

// Handle move a player
func (w *Walk) Handle(params []string) string {
	var newLocation model.LocationActioner
	for _, loc := range w.LocationList {
		if loc.GetTitle() == params[0] {
			newLocation = loc
			break
		}
	}
	if w.Player.Location.HasLocation(&newLocation) == false {
		return fmt.Sprintf("нет пути в %s", params[0])
	}

	r, ok := w.Player.Location.(model.CheckConditionser)

	if ok {
		if err := r.CheckConditions(newLocation); err != nil {
			return err.Error()
		}
	}

	w.Player.ChangeLocation(&newLocation)

	return w.Player.Location.GetStateText() + ". " + w.Player.Location.GetAvalaibleRoutes()
}
