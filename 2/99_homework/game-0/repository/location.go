package repository

import (
	"errors"

	"mail.ru/msu-go-11/2/99_homework/game-0/model"
)

var invRepo = Inventory{}

var hall = model.NewHall("коридор", "ничего интересного")
var kitchen = model.NewKitchen("кухня", ", ничего интересного", "", "кухне")
var room = model.NewLocation("комната", "ты в своей комнате", "пустая комната")
var home = model.NewLocation("домой", "", "")
var outside1 = model.NewLocation("улица", "на улице весна", "")

//LocationRepository a list of avalaible locations
type LocationRepository struct {
	list []model.LocationActioner
}

func init() {
	// hall.SetDoor(model.NewDoor("дверь", invRepo.FindKey()))
	// hall.AddRelatedLocation(kitchen).AddRelatedLocation(room).AddRelatedLocation(outside1)
	// hall.AddCondition(outside1.GetTitle(), func() error {
	// 	if hall.DoorIsOpen() == false {
	// 		return errors.New("дверь закрыта")
	// 	}

	// 	return nil
	// })

	// kitchen.AddRelatedLocation(hall)
	// kitchen.AddRelatedInventory(invRepo.FindKitchenTable())
	// room.AddRelatedLocation(hall)
	// room.AddRelatedInventory(invRepo.FindRoomTable()).AddRelatedInventory(invRepo.FindRoomChair())
	// home.AddRelatedLocation(hall)
	// outside1.AddRelatedLocation(home)
}

var locationList = []model.LocationActioner{
	kitchen,
	room,
	hall,
	home,
	outside1,
}

// NewLocationRepository returns location repository
func NewLocationRepository() *LocationRepository {
	hall.SetDoor(model.NewDoor("дверь", invRepo.FindKey()))
	hall.SetRelatedLocation(kitchen).AddRelatedLocation(room).AddRelatedLocation(outside1)
	hall.AddCondition(outside1.GetTitle(), func() error {
		if hall.DoorIsOpen() == false {
			return errors.New("дверь закрыта")
		}

		return nil
	})

	kitchen.SetRelatedLocation(hall)
	kitchen.SetRelatedInventory(invRepo.FindKitchenTable())
	room.SetRelatedLocation(hall)
	room.SetRelatedInventory(invRepo.FindRoomTable()).AddRelatedInventory(invRepo.FindRoomChair())
	home.SetRelatedLocation(hall)
	outside1.SetRelatedLocation(home)

	return &LocationRepository{
		list: locationList,
	}
}

// FindAll get all rows
func (l *LocationRepository) FindAll() []model.LocationActioner {
	return l.list
}

// FindKitchen returns kitchen
func (l *LocationRepository) FindKitchen() *model.Kitchen {
	return kitchen
}

// FindHall returns kitchen
func (l *LocationRepository) FindHall() *model.Hall {
	return hall
}

// FindRoom returns kitchen
func (l *LocationRepository) FindRoom() *model.Location {
	return room
}

// FindHome returns kitchen
func (l *LocationRepository) FindHome() *model.Location {
	return home
}

// FindOutside1 returns kitchen
func (l *LocationRepository) FindOutside1() *model.Location {
	return outside1
}
