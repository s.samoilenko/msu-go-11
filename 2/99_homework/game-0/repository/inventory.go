package repository

import (
	"mail.ru/msu-go-11/2/99_homework/game-0/model"
)

var backpack = model.NewInventory("рюкзак", "рюкзаке", false, true)
var keys = model.NewInventory("ключи", "", true, false)
var notes = model.NewInventory("конспекты", "", true, false)
var roomTable = model.NewInventory("стол", "на столе:", false, false)
var roomChair = model.NewInventory("стул", "на стуле -", false, false)
var cupOfTea = model.NewInventory("чай", "чае", true, false)
var kitchenTable = model.NewInventory("стол", "на столе", false, false)

var inventoryList = []*model.Inventory{
	&backpack,
	&keys,
	&notes,
	&roomTable,
	&roomChair,
	&kitchenTable,
	&cupOfTea,
}

func init() {
	// roomTable.AddRelatedInventory(&keys).AddRelatedInventory(&notes)
	// roomChair.AddRelatedInventory(&backpack)

	// kitchenTable.AddRelatedInventory(&cupOfTea)
}

// Inventory is a repository class
type Inventory struct {
	list []*model.Inventory
}

// NewInventoryRepository returns a new repo
func NewInventoryRepository() *Inventory {
	roomTable.SetRelatedInventory(&keys).AddRelatedInventory(&notes)
	roomChair.SetRelatedInventory(&backpack)

	kitchenTable.SetRelatedInventory(&cupOfTea)

	return &Inventory{
		list: inventoryList,
	}
}

// FindAll get all available Inventory
func (i *Inventory) FindAll() []*model.Inventory {
	return i.list
}

// FindKey return a key
func (i *Inventory) FindKey() *model.Inventory {
	return &keys
}

// FindRoomTable get the room table
func (i *Inventory) FindRoomTable() *model.Inventory {
	return &roomTable
}

// FindRoomChair get the room table
func (i *Inventory) FindRoomChair() *model.Inventory {
	return &roomChair
}

// FindKitchenTable fina the kitchen table
func (i *Inventory) FindKitchenTable() *model.Inventory {
	return &kitchenTable
}
