package model

// Hall is Hall
type Hall struct {
	Location
	door       *Door
	conditions map[string]func() error
}

// SetDoor assigns a new door
func (l *Hall) SetDoor(door *Door) {
	l.door = door
	l.furnitureList[door.Title] = door
}

// DoorIsOpen checks door is open
func (l *Hall) DoorIsOpen() bool {
	return l.door.isOpen
}

// CheckConditions checks conditions
func (l *Hall) CheckConditions(newLocation LocationActioner) error {
	cond, ok := l.conditions[newLocation.GetTitle()]

	if !ok {
		return nil
	}

	if err := cond(); err != nil {
		return err
	}

	return nil
}

// AddCondition add a new condition
func (l *Hall) AddCondition(key string, condition func() error) *Hall {
	l.conditions[key] = condition

	return l
}

//NewHall creates a new Hall
func NewHall(title, stateText string) *Hall {
	return &Hall{
		Location: Location{
			title:         title,
			stateText:     stateText,
			furnitureList: make(map[string]FurnitureApplyer),
		},
		conditions: make(map[string]func() error),
	}
}
