package model

import (
	"fmt"
	"strings"
)

// Player is  the struct for out player
type Player struct {
	Name     string
	Backpack *Inventory
	Location LocationActioner
}

// LookAround return items around the player
func (p *Player) LookAround() string {
	var res []string

	// return fmt.Sprintf("ты находишься на %s", l.wherePlayerIsTitle)

	if len(p.Location.getIsTitle()) > 0 {
		res = append(res, "ты находишься на "+p.Location.getIsTitle())
	}
	res = append(res, p.Location.getInventorySubsribe())

	tasks := p.Location.getTasks(*p)
	if len(tasks) > 0 {
		res = append(res, "надо "+tasks)
	}

	return (strings.Join(res, ", ") + ". " + p.Location.GetAvalaibleRoutes())
}

func (p *Player) take(i Inventory) {

}

func (p *Player) move(l Location) {

}

func (p *Player) wear(i *Inventory) {
	p.Backpack = i
}

// Put a thing to the backpack
func (p *Player) Put(i *Inventory) {
	p.Backpack.rI = append(p.Backpack.rI, i)
}

// ChangeLocation change a location for the plaer
func (p *Player) ChangeLocation(l *LocationActioner) {
	p.Location = *l
}

// HasInventory is the player has inventory
func (p *Player) HasInventory(i *Inventory) bool {
	if p.Backpack == nil {
		return false
	}

	for _, myI := range p.Backpack.rI {
		if i == myI {
			return true
		}
	}

	return false
}

// PutOn put on a thing
func (p *Player) PutOn(inventoryList []*Inventory, params []string) error {
	var inventoryToWear *Inventory
	for _, tmp := range inventoryList {
		if params[0] == tmp.Title {
			inventoryToWear = tmp
			break
		}
	}

	if inventoryToWear == nil {
		return fmt.Errorf("не такого инвентаря %s", params[0])
	}

	if p.Location.HasInventory(inventoryToWear) == false {
		return fmt.Errorf("не такого инвентаря %s", params[0])
	}

	if inventoryToWear.weareable == false {
		return fmt.Errorf("%s нельзя одеть", params[0])
	}
	p.Location.RemoveInventory(inventoryToWear)
	p.wear(inventoryToWear)

	return nil
}
