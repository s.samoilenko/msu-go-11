package model

import uuid "github.com/nu7hatch/gouuid"

// Furniture is a Furniture
type Furniture struct {
	ID            *uuid.UUID
	Title         string
	TitleGenitive string
}

// FurnitureApplyer is an apply function
type FurnitureApplyer interface {
	Apply(i *Inventory) string
}

// Door is a door
type Door struct {
	Furniture
	isOpen    bool
	keyToOpen *Inventory
}

// Apply inventory
func (d *Door) Apply(i *Inventory) string {
	if d.keyToOpen != i {
		return "не к чему применить"
	}

	if !d.isOpen {
		d.isOpen = true
	}

	return "дверь открыта"
}

// NewDoor creates a new Door instance
func NewDoor(title string, key *Inventory) *Door {
	return &Door{
		Furniture: Furniture{
			Title: title,
		},
		keyToOpen: key,
	}
}
