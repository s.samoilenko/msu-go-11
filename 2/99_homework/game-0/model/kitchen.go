package model

import "strings"

// Kitchen is Kitchen
type Kitchen struct {
	Location
}

func (l *Kitchen) getTasks(p Player) string {
	var res []string

	if p.Backpack == nil {
		res = append(res, "собрать рюкзак")
	}

	res = append(res, "идти в универ")

	return strings.Join(res, " и ")
}

// NewKitchen returns a new kitchen
func NewKitchen(title, stateText, emptyLocationText, wherePlayerIsTitle string) *Kitchen {
	kitchen := &Kitchen{
		Location: *NewLocation(title, title+stateText, emptyLocationText),
	}

	kitchen.WherePlayerIsTitle = wherePlayerIsTitle

	return kitchen
}
