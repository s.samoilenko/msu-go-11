package model

import (
	"github.com/nu7hatch/gouuid"
)

// Inventory base structure
type Inventory struct {
	id            *uuid.UUID
	Title         string
	titleGenitive string
	takeble       bool
	weareable     bool
	rI            []*Inventory
}

// InventoryActions avalaible inventory actions
type InventoryActions interface {
	canTake() bool
	canWear() bool
}

// CanTake thing or not
func (i *Inventory) CanTake() bool {
	return i.takeble
}

func (i *Inventory) canWear() bool {
	return i.weareable
}

// AddRelatedInventory takes an inventory as parameter
func (i *Inventory) AddRelatedInventory(ri *Inventory) *Inventory {
	i.rI = append(i.rI, ri)

	return i
}

// SetRelatedInventory takes an inventory as parameter
func (i *Inventory) SetRelatedInventory(ri *Inventory) *Inventory {
	i.rI = i.rI[:0]

	return i.AddRelatedInventory(ri)
}

// NewInventory create a new Inventory
func NewInventory(title, titleGenitive string, takeble, weareable bool) Inventory {
	id, _ := uuid.NewV4()

	return Inventory{
		takeble:       takeble,
		weareable:     weareable,
		id:            id,
		Title:         title,
		titleGenitive: titleGenitive,
	}
}
