package model

import (
	"strings"
)

// CheckConditionser is CheckConditionser
type CheckConditionser interface {
	CheckConditions(newLocation LocationActioner) error
}

// LocationActioner is LocationTsker
type LocationActioner interface {
	getTasks(p Player) string
	getIsTitle() string
	getInventorySubsribe() string
	GetAvalaibleRoutes() string
	GetStateText() string
	HasLocation(t *LocationActioner) bool
	HasInventory(i *Inventory) bool
	RemoveInventory(inventory *Inventory)
	Titler
	GetFurnitureList() map[string]FurnitureApplyer
}

// Titler is Titler
type Titler interface {
	GetTitle() string
}

// Location structure
type Location struct {
	title string
	// rl                 map[string]*Location
	rl                 []Titler
	ri                 []*Inventory
	WherePlayerIsTitle string
	// onTitle            string
	emptyLocationText string
	tasks             []string
	stateText         string
	furnitureList     map[string]FurnitureApplyer
}

// GetFurnitureList returns a furniture list
func (l *Location) GetFurnitureList() map[string]FurnitureApplyer {
	return l.furnitureList
}

// GetStateText returns a state text
func (l *Location) GetStateText() string {
	return l.stateText
}

// GetTitle get a name of location
func (l Location) GetTitle() string {
	return l.title
}

func (l *Location) getIsTitle() string {
	return l.WherePlayerIsTitle
}

func (l *Location) getTasks(p Player) string {
	return ""
}

// AddRelatedLocation add a new related location
func (l *Location) AddRelatedLocation(rL Titler) *Location {
	// l.rl[rL.title] = rL

	l.rl = append(l.rl, rL)
	// rL.rl = append(rL.rl, l)
	return l
}

// SetRelatedLocation add a new related location
func (l *Location) SetRelatedLocation(rL Titler) *Location {
	l.rl = l.rl[:0]
	return l.AddRelatedLocation(rL)
}

// AddRelatedInventory add a new related inventory
func (l *Location) AddRelatedInventory(i *Inventory) *Location {
	l.ri = append(l.ri, i)

	return l
}

// SetRelatedInventory add a new related inventory
func (l *Location) SetRelatedInventory(i *Inventory) *Location {
	l.ri = l.ri[:0]

	return l.AddRelatedInventory(i)
}

// HasLocation returns is location exists or not
func (l *Location) HasLocation(newLocation *LocationActioner) bool {
	for _, loc := range l.rl {
		b := *newLocation
		a := b.(Titler)
		if loc == a {
			return true
		}
	}

	return false
	// _, ok := l.rl[title]

	// return ok
}

// GetAvalaibleRoutes return a list of routes
func (l *Location) GetAvalaibleRoutes() string {
	var res []string
	for _, loc := range l.rl {
		res = append(res, loc.GetTitle())
	}

	return "можно пройти - " + strings.Join(res, ", ")
}

func (l *Location) move() {

}

func (l *Location) isInventoryInLocation(ri []*Inventory, lookingForInventory *Inventory) bool {
	for _, inv := range ri {
		if lookingForInventory == inv {
			return true
		}

		if len(inv.rI) > 0 {
			if l.isInventoryInLocation(inv.rI, lookingForInventory) {
				return true
			}
		}
	}

	return false
}

// RemoveInventoryFromInventory from a certain location
func (l *Location) RemoveInventoryFromInventory(invSlice1 *[]*Inventory, inventory *Inventory) bool {
	invSlice := *invSlice1
	for i, inv := range invSlice {
		if inv == inventory {
			invSlice = append(
				invSlice[:i],
				invSlice[i+1:]...,
			)
			*invSlice1 = invSlice
			return true
		}

		if len(inv.rI) > 0 {
			if l.RemoveInventoryFromInventory(&inv.rI, inventory) == true {
				return true
			}
		}
	}

	return false
}

//RemoveInventory from location
func (l *Location) RemoveInventory(inventory *Inventory) {
	l.RemoveInventoryFromInventory(&l.ri, inventory)
}

// HasInventory checks a location has inventory or not
func (l *Location) HasInventory(lookingForInventory *Inventory) bool {
	return l.isInventoryInLocation(l.ri, lookingForInventory)
}

func (l *Location) getInventorySubsribe() string {
	var res []string
	for _, furniture := range l.ri {
		var inventoryOnFurtniture []string
		for _, inventory := range furniture.rI {
			inventoryOnFurtniture = append(inventoryOnFurtniture, inventory.Title)
		}

		if len(inventoryOnFurtniture) > 0 {
			res = append(res, furniture.titleGenitive+" "+strings.Join(inventoryOnFurtniture, ", "))
		}

	}

	if len(res) > 0 {
		return strings.Join(res, ", ")
	}

	// l.onTitle +
	return l.emptyLocationText
}

func (l *Location) shouldDo() string {
	return strings.Join(l.tasks, " ")
}

// NewLocation creates a new Location structure
func NewLocation(title, stateText, emptyLocationText string) *Location {
	// return &Location{rl: make(map[string]*Location)}
	return &Location{
		title:             title,
		stateText:         stateText,
		emptyLocationText: emptyLocationText,
	}
}
