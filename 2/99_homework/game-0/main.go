package main

import (
	"strings"

	"mail.ru/msu-go-11/2/99_homework/game-0/commands"
	"mail.ru/msu-go-11/2/99_homework/game-0/model"
	"mail.ru/msu-go-11/2/99_homework/game-0/repository"
)

var commandList map[string]commands.CommandHandle

func initGame() {
	var invRepo = repository.NewInventoryRepository()
	var inventoryList = invRepo.FindAll()

	var locRepo = repository.NewLocationRepository()
	var locationList = locRepo.FindAll()

	var kitchen = locRepo.FindKitchen()
	player := &model.Player{Name: "Sergey", Location: kitchen}

	commandList = make(map[string]commands.CommandHandle)
	commandList["осмотреться"] = &commands.LookAround{Player: player}
	commandList["идти"] = &commands.Walk{Player: player, LocationList: locationList}
	commandList["одеть"] = &commands.PutOn{Player: player, InventoryList: inventoryList}
	commandList["взять"] = &commands.Take{Player: player, InventoryList: inventoryList}
	commandList["применить"] = &commands.Apply{Player: player, InventoryList: inventoryList}
}

func handleCommand(command string) string {
	if len(command) == 0 {
		return "The command is empty"
	}

	var parts = strings.Split(command, " ")

	// currentCommand, ok := commands[parts[0]]

	currentCommand, ok := commandList[parts[0]]

	if ok == false {
		return "неизвестная команда"
	}

	return currentCommand.Handle(parts[1:])

	// return strings.Join(parts[1:], ", ")
}

// func main() {
// 	type gameCase struct {
// 		step    int
// 		command string
// 		answer  string
// 	}

// 	var game0cases = [][]gameCase{
// 		[]gameCase{
// 			{1, "осмотреться", "ты находишься на кухне, на столе чай, надо собрать рюкзак и идти в универ. можно пройти - коридор"},
// 			{2, "завтракать", "неизвестная команда"},  // придёт топать в универ голодным :(
// 			{3, "идти комната", "нет пути в комната"}, // через стены ходить нельзя
// 			{4, "идти коридор", "ничего интересного. можно пройти - кухня, комната, улица"},
// 			{5, "применить ключи дверь", "нет предмета в инвентаре - ключи"},
// 			{6, "идти комната", "ты в своей комнате. можно пройти - коридор"},
// 			{7, "осмотреться", "на столе: ключи, конспекты, на стуле - рюкзак. можно пройти - коридор"},
// 			{8, "взять ключи", "некуда класть"}, // надо взять рюкзак сначала
// 			{9, "одеть рюкзак", "вы одели: рюкзак"},
// 			{10, "осмотреться", "на столе: ключи, конспекты. можно пройти - коридор"}, // состояние изменилось
// 			{11, "взять ключи", "предмет добавлен в инвентарь: ключи"},
// 			{12, "взять телефон", "нет такого"},                                // неизвестный предмет
// 			{13, "взять ключи", "нет такого"},                                  // предмента уже нет в комнатеы - мы его взяли
// 			{14, "осмотреться", "на столе: конспекты. можно пройти - коридор"}, // состояние изменилось
// 			{15, "взять конспекты", "предмет добавлен в инвентарь: конспекты"},
// 			{16, "осмотреться", "пустая комната. можно пройти - коридор"}, // состояние изменилось
// 			{17, "идти коридор", "ничего интересного. можно пройти - кухня, комната, улица"},
// 			{18, "идти кухня", "кухня, ничего интересного. можно пройти - коридор"},
// 			{19, "осмотреться", "ты находишься на кухне, на столе чай, надо идти в универ. можно пройти - коридор"}, // состояние изменилось
// 			{20, "идти коридор", "ничего интересного. можно пройти - кухня, комната, улица"},
// 			{21, "идти улица", "дверь закрыта"},                                  //условие не удовлетворено
// 			{22, "применить ключи дверь", "дверь открыта"},                       //состояние изменилось
// 			{23, "применить телефон шкаф", "нет предмета в инвентаре - телефон"}, // нет предмета
// 			{24, "применить ключи шкаф", "не к чему применить"},                  // предмет есть, но применить его к этому нельзя
// 			{25, "идти улица", "на улице весна. можно пройти - домой"},
// 		},
// 	}
// 	// var m map[Inventory][]Inventory

// 	// fmt.Println(m)

// 	for caseNum, commands := range game0cases {
// 		initGame()
// 		for _, item := range commands {
// 			answer := handleCommand(item.command)
// 			if answer != item.answer {
// 				fmt.Printf("%d = %d", len(answer), len(item.answer))
// 				fmt.Println("case:", caseNum, item.step,
// 					"\n\tcmd:", item.command,
// 					"\n\tresult:  ", answer,
// 					"\n\texpected:", item.answer)
// 			}
// 		}
// 	}

// }
