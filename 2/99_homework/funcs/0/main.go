package main

import (
	"fmt"
	"math"
)

// Sqrt TODO: Реализовать вычисление Квадратного корня
func Sqrt(x float64) float64 {
	if x < 0 {
		x = x * (-1)
	}
	var step = x / 2

	for math.Abs(step*step-x) > 0.01 {
		step = (step*step + x) / 2 / step
	}

	return step
}

func main() {
	res := Sqrt(-1)
	fmt.Println("============")
	fmt.Println(res)
}
