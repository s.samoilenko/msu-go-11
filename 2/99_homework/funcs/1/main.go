package main

import (
	"fmt"
	"strconv"
)

type memoizeFunction func(int, ...int) interface{}

// type memoizeFunction func(int, ...int) int

// TODO реализовать
var fibonacci memoizeFunction = func(c int, b ...int) interface{} {
	fmt.Println(b)
	return c
}
var romanForDecimal memoizeFunction = func(c int, b ...int) interface{} {
	fmt.Println(b)
	return strconv.Itoa(c)
}

//TODO Write memoization function

func memoize(function memoizeFunction) memoizeFunction {
	var s []int
	return func(c int, d ...int) interface{} {
		s = append(s, c)
		return function(c-1, s...)
	}
}

// TODO обернуть функции fibonacci и roman в memoize
func init() {
	fibonacci = memoize(fibonacci)
	romanForDecimal = memoize(romanForDecimal)
}

func main() {
	fmt.Println("Fibonacci(45) =", fibonacci(45).(int))
	for _, x := range []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
		14, 15, 16, 17, 18, 19, 20, 25, 30, 40, 50, 60, 69, 70, 80,
		90, 99, 100, 200, 300, 400, 500, 600, 666, 700, 800, 900,
		1000, 1009, 1444, 1666, 1945, 1997, 1999, 2000, 2008, 2010,
		2012, 2500, 3000, 3999} {
		fmt.Printf("%4d = %s\n", x, romanForDecimal(x).(string))
	}
}
