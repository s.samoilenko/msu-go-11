package main

import "time"
import "fmt"

// Calendar struct for the representionf name
type Calendar struct {
	parsedDate time.Time
}

//CurrentQuarter get current quater
func (r *Calendar) CurrentQuarter() int {
	m := int(r.parsedDate.Month())

	if m%3 == 0 {
		return m / 3
	}

	return (m / 3) + 1

}

// NewCalendar create a new calendar
func NewCalendar(parsedDate time.Time) Calendar {
	return Calendar{parsedDate}
}

func main() {
	parsed, _ := time.Parse("2006-01-02", fmt.Sprintf("2015-%s-15", "01"))
	calendar := NewCalendar(parsed)
	calendar.CurrentQuarter()
}
