package main

import (
	"sort"
	"strconv"
)

func main() {

}

// ReturnInt this function allways returns 1
func ReturnInt() int {
	return 1
}

// ReturnFloat this function always return 1.1
func ReturnFloat() float32 {
	return 1.1
}

// ReturnIntArray returns array
func ReturnIntArray() [3]int {
	return [3]int{1, 3, 4}
}

// ReturnIntSlice returns a slice
func ReturnIntSlice() []int {
	return []int{1, 2, 3}
}

// IntSliceToString slice to string
func IntSliceToString(inSlice []int) string {
	var res string
	for _, v := range inSlice {
		res += strconv.Itoa(v)
	}

	return res
}

// MergeSlices convert
func MergeSlices(s1 []float32, s2 []int32) []int {
	var res []int

	for _, v := range s1 {
		res = append(res, int(v))
	}

	for _, v := range s2 {
		res = append(res, int(v))
	}

	return res
}

// GetMapValuesSortedByKey sort map
func GetMapValuesSortedByKey(m map[int]string) []string {
	var keys []int
	var res = make([]string, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	for _, k := range keys {
		res = append(res, m[k])
	}

	return res
}
