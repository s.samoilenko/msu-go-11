package main

import (
	"time"

	cake "mail.ru/msu-go-11/3/Code/ch8/cake/cake-factory"
)

var defaults = cake.Shop{
	Verbose:      true,
	Cakes:        1,
	BakeTime:     10 * time.Millisecond,
	NumIcers:     1,
	IceTime:      10 * time.Millisecond,
	InscribeTime: 10 * time.Millisecond,
	BakeBuf:      1,
}

func main() {
	cakeshop := defaults
	cakeshop.Work(1) // 224 ms
}
