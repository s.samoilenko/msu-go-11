// Copyright © 2016 Alan A. A. Donovan & Brian W. Kernighan.
// License: https://creativecommons.org/licenses/by-nc-sa/4.0/

// See page 228.

// Pipeline1 demonstrates an infinite 3-stage pipeline.
package main

import "fmt"

//!+
func main() {
	naturals := make(chan int)
	squares := make(chan int)

	// Counter
	go func() {
		defer close(naturals)
		for x := 0; x < 10; x++ {
			naturals <- x
		}
	}()

	// Squarer
	go func() {
		defer close(squares)
		for {
			x, ok := <-naturals

			if !ok {
				return
			}
			squares <- x * x
		}
	}()

	go func() {
		for {
			x, ok := <-naturals

			if !ok {
				return
			}
			fmt.Printf("Natural: %d \n", x)
		}
	}()

	// Printer (in main goroutine)
	for {
		c, ok := <-squares
		if !ok {
			break
		}
		fmt.Println(c)
	}
}

//!-
