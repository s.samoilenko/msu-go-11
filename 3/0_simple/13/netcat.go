// Copyright © 2016 Alan A. A. Donovan & Brian W. Kernighan.
// License: https://creativecommons.org/licenses/by-nc-sa/4.0/

// See page 221.
//!+

// Netcat1 is a read-only TCP client.
package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strings"
)

func main() {
	args := os.Args[1:]
	if len(args) < 1 {
		fmt.Println("Exit, Parameters did not received")
		return
	}
	// for {
	for _, param := range args {
		parts := strings.Split(param, "=")

		go connectToClock(parts[0], parts[1])
	}

	for {
	}
	// }
}

// MyReader is MyReader
type MyReader struct {
	city  string
	clock io.Reader
}

func (r *MyReader) Read(b []byte) (int, error) {
	n, err := r.clock.Read(b)
	r.clock.Read(b)
	time := make([]byte, n, n)
	copy(time, b[:n])
	byteCity := []byte(r.city + ": ")

	lenCity := len(byteCity)
	for i := 0; i < lenCity; i++ {
		b[i] = byteCity[i]
	}

	for i := lenCity; i < (n + lenCity); i++ {
		b[i] = time[i-lenCity]
	}

	return n + lenCity, err
}

func connectToClock(city string, address string) {
	conn, err := net.Dial("tcp", address)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	reader := MyReader{
		city:  city,
		clock: conn,
	}

	mustCopy(os.Stdout, &reader)
}

func mustCopy(dst io.Writer, src io.Reader) {
	if _, err := io.Copy(dst, src); err != nil {
		log.Fatal(err)
	}
}

//!-
