package main

import (
	//	"fmt"

	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	r io.Reader
}

func (r *rot13Reader) Read(b []byte) (int, error) {
	//fmt.Println(r.r)
	n, err := r.r.Read(b)

	for i := 0; i < n; i++ {
		if b[i] >= 65 && b[i] <= 90 {
			b[i] = b[i] + 13
			if b[i] > 90 {
				b[i] = b[i] - 90 + 64
			}
			continue
		}

		if b[i] >= 97 && b[i] <= 122 {
			b[i] = b[i] + 13
			if b[i] > 122 {
				b[i] = b[i] - 122 + 96
			}
			continue
		}

		b[i] = b[i] + 13

	}

	//	fmt.Println(n)

	return n, err
}

func main() {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}

	io.Copy(os.Stdout, &r)
}
