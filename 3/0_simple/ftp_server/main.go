package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	listener, err := net.Listen("tcp", "localhost:8000")
	fmt.Println("Listen to localhost:8022")
	if err != nil {
		log.Fatal(err)
	}
	for {
		conn, err := listener.Accept()
		fmt.Println("Connected")
		io.WriteString(conn, "Connected")
		if err != nil {
			log.Print(err)
			continue
		}

		handelConn(conn)
	}
}

func handelConn(c net.Conn) {
	defer c.Close()

	var (
		// buf bytes.Buffer
		buf = make([]byte, 1024)
		r   = bufio.NewReader(c)
		w   = bufio.NewWriter(c)
	)

	for {
		// fmt.Println("Waiting")

		// w.Write([]byte("pong"))
		// w.Flush()

		n, err := r.Read(buf)

		if err != nil {
			fmt.Println(err)
			break
		}

		data := string(buf[:n])

		log.Println("Receive:", data)

		message := getResponse(strings.TrimSpace(data))

		w.Write([]byte(message))
		w.Flush()

		log.Printf("Send: %s", message)
	}
}

func getResponse(command string) string {
	switch command {
	case "pwd":
		dir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
		return dir
	case "ls":
		files, _ := ioutil.ReadDir("./")
		var res []string
		for _, f := range files {
			res = append(res, f.Name())
		}
		return strings.Join(res, "\n")
	default:
		return "EOF"
	}
}
