package main

import (
	"fmt"
	"io"
	"math"
	"net"
)

// Calculator is Calculator
type Calculator struct {
	acc float64
}

// Do is Do
func (c *Calculator) Do(op func(float64) float64) float64 {
	c.acc = op(c.acc)

	return c.acc
}

// Add is Add
func Add(n float64) func(float64) float64 {
	return func(c float64) float64 {
		return c + n
	}
}

// Sqrt is Sqrt
func Sqrt() func(float64) float64 {
	return func(c float64) float64 {
		return math.Sqrt(c)
	}
}

// Mux is Mux
type Mux struct {
	add     chan net.Conn
	remove  chan net.Addr
	sendMsg chan string
	ops     chan func(map[net.Addr]net.Conn)
}

//Add is Add
func (m *Mux) Add(conn net.Conn) {
	m.ops <- func(m map[net.Addr]net.Conn) {
		m[conn.RemoteAddr()] = conn
	}
}

// Remove  is Remove
func (m *Mux) Remove(addr net.Addr) {
	m.ops <- func(m map[net.Addr]net.Conn) {
		delete(m, addr)
	}
}

// SendMsg is SendMsg
func (m *Mux) SendMsg(msg string) error {
	result := make(chan error, 1)
	m.ops <- func(m map[net.Addr]net.Conn) {
		for _, conn := range m {
			if _, err := io.WriteString(conn, msg); err != nil {
				result <- err
				return
			}
		}

		result <- nil
	}
	return <-result
}

// PrivateMsg is PrivateMsg
func (m *Mux) PrivateMsg(addr net.Addr, msg string) error {
	result := make(chan net.Conn, 1)
	m.ops <- func(m map[net.Addr]net.Conn) {
		result <- m[addr]
	}

	conn := <-result

	if conn == nil {
		return fmt.Errorf("client %v not registered", addr)
	}

	_, err := io.WriteString(conn, msg)

	return err
}

func (m *Mux) loop() {
	conns := make(map[net.Addr]net.Conn)
	for op := range m.ops {
		op(conns)
	}
}

func main() {
	c := Calculator{}
	fmt.Println(c.Do(Add(5)))
	fmt.Println(c.Do(Add(5)))
	fmt.Println(c.Do(math.Sqrt))
}
