package main

// RoundRobinBalancer is a loade balanser
type RoundRobinBalancer struct {
	servers      []int
	current      int
	giveNOdeChan chan int
	currentChan  chan int
}

func (r RoundRobinBalancer) run() {
	var item int
	for {
		select {
		case item = <-r.giveNOdeChan:
			r.servers[r.current%len(r.servers)] += item
			old := r.current

			r.current += item
			r.currentChan <- old
		}
	}
}

// Init инициализирует собственно балансер - представьте что устанавливает соединения с указанным колчиеством серверов.
func (r *RoundRobinBalancer) Init(len int) {
	r.servers = make([]int, len)
	r.currentChan = make(chan int)
	r.giveNOdeChan = make(chan int)
	go r.run()
	// r.current = 1
}

// GiveStat даёт статистику, сколько запросов пришло на каждый из серверов.
func (r *RoundRobinBalancer) GiveStat() []int {
	return r.servers
}

// GiveNode эта функция фвзывается, когда пришел запрос. мы получаем номер сервера, на который идти.
func (r *RoundRobinBalancer) GiveNode() int {
	r.giveNOdeChan <- 1

	return <-r.currentChan
}

func main() {

}
