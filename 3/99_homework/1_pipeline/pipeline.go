// package main
package pipeline

type job func(in, out chan interface{})

func secondPipe(f job, in chan interface{}, c chan int) chan interface{} {
	out := make(chan interface{})
	go func() {
		f(in, out)

		close(out)
		c <- 1
	}()

	return out
}

// Pipe os a pipe
func Pipe(funcs ...job) {
	counter := make(chan int, len(funcs))
	var in chan interface{}

	for _, f := range funcs {
		in = secondPipe(f, in, counter)
	}

	for range funcs {
		<-counter
	}

	return
}
